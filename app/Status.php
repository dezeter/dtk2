<?php

namespace App;

use App\Services;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    public $timestamps = false;

    protected $fillable = [
    	'status_name',
    ];

    public function services(){
    	return $this->belongTo('App\Services');
    }

    public function getContent($key){
  		return $this->getAttribute($key);
  }
}
