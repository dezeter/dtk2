<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersServices extends Model
{
    protected $table = 'orders_services';

    protected $fillable = [
    	'orders_id',
    	'services_id',
    	'status_id',
    ];

	public function getContent($key){
  		return $this->getAttribute($key);
    }

    public function status(){
        return $this->belongsTo('App\Status');
    }

    public function service(){
        return $this->belongsTo('App\Services', 'services_id');
    }

}
