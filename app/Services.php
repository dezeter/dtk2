<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';

    protected $fillable = [
    	'service_name',
    	'service_discription',
    	'price',
    ];

	public function orders(){
        return $this->belongsToMany('App\Orders')->withPivot('id', 'status_id');
    }

    public function getContent($key){
  		return $this->getAttribute($key);
    }

    public function status(){
        return $this->hasOne('App\Status');
    }
}
