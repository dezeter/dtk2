<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dtkComments extends Model
{
	protected $table = 'dtk_comments';

	protected $fillable = [
		'user_id',
		'order_id',
		'text',
	];

	public function getContent($key){
		return $this->getAttribute($key);
	}
}
