<?php

namespace App;

use Orchid\Platform\Core\Models\User as BaseUser;

class User extends BaseUser
{

	protected $table = 'users';

	protected $fillable = [
		'name',
		'email',
		'password',
		'permisssions',
	];
}