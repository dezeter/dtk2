<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportOrders extends Table
{

    /**
     * @var string
     */
    public $data = 'orders';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('count')
                ->title('Всего заявок')
                ->setRender(function ($row){
                    return $row->count();}
            ),
        ];
    }
}
