<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportAll extends Table
{

    /**
     * @var string
     */
    public $data = 'all';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('all')
                ->title('Всего услуг')
                ->setRender(function ($row){
                    return $row;
                }),
        ];
    }
}
