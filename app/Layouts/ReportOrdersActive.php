<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportOrdersActive extends Table
{

    /**
     * @var string
     */
    public $data = 'orders';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('count')
                ->title('Заявок в работе')
                ->setRender(function ($row){
                    return $row->where('status_ended', 0)->count();
                }),
        ];
    }
}
