<?php

namespace App\Layouts;

use App\Status;
use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Platform\Fields\TD;
use Orchid\Platform\Fields\Field;

class StatusRowsSelect extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('select')
        				->options(Status::all()->pluck("status_name", "id"))
        				->name('ordersservices.status_id')
        				->required()
        				->title('Название статуса'),
        ];
    }
}
