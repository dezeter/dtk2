<?php

namespace App\Layouts;

use App\User;
use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class CommentsList extends Table
{

    /**
     * @var string
     */
    public $data = 'comments';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('user_id')
                ->title('Автор')
                ->setRender(function ($row) {
                    return User::where('id', $row->user_id)->first()->name;
                }),

            TD::name('text')
                ->title('текст'),
        ];
    }
}
