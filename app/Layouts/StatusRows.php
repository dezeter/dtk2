<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Platform\Fields\TD;
use Orchid\Platform\Fields\Field;

class StatusRows extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('input')
        				->type('text')
        				->name('status.status_name')
        				->required()
                        ->maxlength('70')
        				->title('Название статуса'),
        ];
    }
}
