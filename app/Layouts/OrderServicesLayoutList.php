<?php

namespace App\Layouts;


use App\Status;
use App\Services;
use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;
use Orchid\Platform\Screen\Link;

class OrderServicesLayoutList extends Table
{

    /**
     * @var string
     */
    public $data = 'services';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')
                ->title('Название услуги')
                ->setRender(function ($row){
                    return Services::where('id', $row->id)->first()->service_name;
                }),

            TD::name('price')
                ->title('стоимость')
                ->setRender(function ($row){
                    return $row->price . ' руб.';
                }),

            TD::name('')
                ->title('Текущий статус')
                ->setRender(function ($row){
                    return Status::where('id', $row->pivot->status_id)->first()->status_name;
                }),
            TD::name('')
                ->title('')
                ->width(200)
                ->setRender(function ($row){ 
                    return '<a href="' . route('dashboard.ordersystem.ordersservices.edit',  $row->pivot->id) . '"> Изменить статус </a>';
                }),
        ];
    }
}
