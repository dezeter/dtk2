<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Platform\Fields\TD;

class OrderRows extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('textarea')
        		->name('orders.order_discription')
        		->reqired()
        		->title('Описание'),
        ];
    }
}
