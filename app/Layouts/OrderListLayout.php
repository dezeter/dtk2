<?php

namespace App\Layouts;

use App\User;
use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class OrderListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'orders';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('id')
                ->title('Номер завяки'),

            TD::name('user_id')
                ->title('Имя пользователя')
                ->setRender(function ($row) {
                    return User::where('id', $row->user_id)->first()->name;
                }),

            TD::name('order_discription')
                ->title('доп. инфо'),

            TD::name('')
                ->title('')
                ->width(200)
                ->setRender(function ($row) {
                    return '<a href="' . route('dashboard.ordersystem.orders.edit',
                            $row->id) . '"> Управление </a>';
                }),
        ];
    }
}
