<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportClients extends Table
{

    /**
     * @var string
     */
    public $data = 'clients';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('clients')
                ->title('Всего клиентов')
                ->setRender(function ($row){
                    return $row;
                }),
        ];
    }
}
