<?php

namespace App\Layouts;

use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class ServiceRows extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('input')
        			->type('text')
        			->name('services.service_name')
        			->maxlength('255')
        			->required()
        			->title('Название услгуи')
        			->help('Название услуги'),

        	Field::tag('textarea')
        			->name('services.service_discription')
                    ->title('Описание услуги'),

        	Field::tag('input')
        			->type('text')
        			->name('services.price')
                    ->maxlength('10')
                    ->pattern('[0-9]{3}')
                    ->title('Цена'),
        ];
    }
}
