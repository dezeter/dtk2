<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportComments extends Table
{

    /**
     * @var string
     */
    public $data = 'allomments';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('allomments')
                ->title('Оставленно коментариев')
                ->setRender(function ($row){
                    return $row;
                }),
        ];
    }
}
