<?php

namespace App\Layouts;


use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class StatusListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'status';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('status_name')
                ->title('Название Статуса'),

            TD::name('')
                ->title('')
                ->width(200)
                ->setRender(function ($row) {
                    return '<a href="' . route('dashboard.ordersystem.status.edit',
                            $row->id) . '"> Редактировать/Удалить </a>';
                }),
        ];
    }
}
