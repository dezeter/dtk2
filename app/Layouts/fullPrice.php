<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Platform\Fields\TD;

class fullPrice extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('input')
        		->name('full_price')
        		->reqired()
        		->value(old('sum'))
        		->title('Общая стоимость')
        ];
    }
}
