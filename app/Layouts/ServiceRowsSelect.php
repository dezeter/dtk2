<?php

namespace App\Layouts;

use App\Services;
use App\Orders;
use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Platform\Fields\TD;
use Orchid\Platform\Fields\Field;

class ServiceRowsSelect extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('select')
        				->options(Services::all()->pluck("service_name", "id"))
        				->name('service_id')
        				->required()
        				->title('Название статуса'),
        ];
    }
}
