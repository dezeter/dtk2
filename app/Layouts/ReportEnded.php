<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ReportEnded extends Table
{

    /**
     * @var string
     */
    public $data = 'ended';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('ended')
                ->title('Выполненных услуг')
                ->setRender(function ($row){
                    return $row;
                }),
        ];
    }
}
