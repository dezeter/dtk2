<?php

namespace App\Layouts;

use App\Status;
use Orchid\Platform\Layouts\Table;
use Orchid\Platform\Platform\Fields\TD;

class ServiceListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'services';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::name('service_name')
                ->title('Название услуги'),

            TD::name('service_discription')
                ->title('описание услуги'),

            TD::name('price')
                ->title('Цена')
                ->setRender(function ($row){
                    return $row->price . " руб.";
                }),
                
            TD::name('')
                ->title('')
                ->width(200)
                ->setRender(function ($row) {
                    return '<a href="' . route('dashboard.ordersystem.services.edit',
                            $row->id) . '"> Редактировать/Удалить </a>';
                }),
        ];
    }
}
