<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Platform\Fields\TD;

class AddComment extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
        	Field::tag('button-link')
        		->name('comments.text')
                ->id('show-button-modal-Service')
        		->href('http://dtk2.oo/dashboard/screen/orders/18/edit/addComments')
                ->title('Добавить коментарий')
        		->class("btn btn-default pull-right")
                ->mothod('babab')
        ];
    }
}
