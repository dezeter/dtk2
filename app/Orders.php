<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';

    protected $fillable = [
    	'order_discription',
    	'user_id',
    ];

    public function getContent($key){
  		return $this->getAttribute($key);
  	}

    public function services() {
        return $this->belongsToMany('App\Services')->withPivot('id', 'status_id');
    }

    public function order_services() {
        return $this->hasMany('App\OrdersServices')->with("status", "service");
    }

    public function user() {
        return $this->hasOne('App\User');
    }
}
