<?php

namespace App\Http\Composer;

use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Kernel\Dashboard;
use Orchid\Platform\Notifications\DashboardNotification;

class MenuComposer
{
    /**
     * MenuComposer constructor.
     *
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     *
     */
    public function compose()
    {
        $this->dashboard->menu->add('Main', [
            'slug'   => 'Custom',
            'icon'   => 'icon-drop',
            'route'  => '#',
            'active' => 'dashboard.ordersystem.*',
            'label'  => 'Учет заказов на услуги',
            'childs' => true,
            'main'   => true,
            'sort'   => 0,
        ]);

        $this->dashboard->menu->add('Custom', [
            'slug'   => 'Element',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.services.list'),
            'active' => 'dashboard.ordersystem.services.*',
            'label'  => 'Услуги',
            'divider'=> false,
            'childs' => false,
            'sort'   => 1,
        ]);

        $this->dashboard->menu->add('Custom', [
            'slug'   => 'Element2',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.status.list'),
            'active' => 'dashboard.ordersystem.status.*',
            'label'  => 'Статус',
            'divider'=> false,
            'childs' => false,
            'sort'   => 2,
        ]);

        $this->dashboard->menu->add('Custom', [
            'slug'   => 'Element3',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.orders.list'),
            'active' => 'dashboard.ordersystem.orders.*',
            'label'  => 'Заявки',
            'divider'=> false,
            'childs' => false,
            'sort'   => 2,
        ]);

        $this->dashboard->menu->add('Custom', [
            'slug'   => 'Orders',
            'icon'   => 'icon-user-female',
            'route'  => '#',
            'label'  => 'Отчеты',
            'childs' => true,
            'main'   => true,
            'sort'   => 2,
        ]);
        $this->dashboard->menu->add('Orders', [
            'slug'   => 'Order1',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.reports1'),
            'active' => 'dashboard.ordersystem.orders.*',
            'label'  => 'Заявки',
            'childs' => false,
            'main'   => false,
            'sort'   => 2,
        ]);
        $this->dashboard->menu->add('Orders', [
            'slug'   => 'Order2',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.reports2'),
            'active' => 'dashboard.ordersystem.orders.*',
            'label'  => 'Услуги',
            'childs' => false,
            'main'   => false,
            'sort'   => 2,
        ]);
        $this->dashboard->menu->add('Orders', [
            'slug'   => 'Order3',
            'icon'   => 'icon-user-female',
            'route'  => route('dashboard.ordersystem.reports3'),
            'active' => 'dashboard.ordersystem.orders.*',
            'label'  => 'Данные',
            'childs' => false,
            'main'   => false,
            'sort'   => 2,
        ]);


    }
}