<?php

namespace App\Http\Controllers\Screens\OrderSystem\Orders;

use App\Orders;
use App\dtkComments;
use App\Layouts\OrderRows;
use App\Layouts\OrderServicesLayoutList;
use App\Layouts\ServiceRowsSelect;
use App\Layouts\StatusRowsSelect;
use App\Layouts\CommentsList;
use App\Layouts\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Facades\Alert;


class OrderEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Заявка';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Создание/Редактирование заявки';

    /**
     * Query data
     *
     * @return array
     */

    public function query($orders = null) : array
    {
        //$orders = Orders::where('id', $orders)->first();
        $orders = is_null($orders) ? new Orders() : $orders;
        $dtkcomments = dtkComments::where('order_id', $orders->id);
        $services = $orders->services()->orderBy('id')->paginate(10);
        $sum = $orders->services()->sum('price');
                
        return [
            'orders'        => $orders,
            'services'      => $services,
            'comments'      => $dtkcomments->orderBy('id')->paginate(10),
            'sum'           => $sum,
        ];
    }



    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Сохранить')->method('save'),
            Link::name('Удалить')->method('remove'),
            Link::name('Закрыть и выставить счет')->method('fullPrice'),
            Link::name('Добавить услугу')
                ->modal('Service')
                ->title('Добавление услуги')
                ->method('addService'),
            Link::name('Добавить Коментарий')
                ->modal('Comments')
                ->title('Добавить коментарий')
                ->method('addComments'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            Layouts::tabs([
                'Услуги' => [
                    OrderServicesLayoutList::class,
                ]
            ]),

            Layouts::columns([
                'Left column' => [
                    OrderRows::class,
                ],
                'Right column' => [
                    CommentsList::class,
                    /** AddComment::class,*/
                ],
            ]),
            
            Layouts::modals([
                'Service'  => [
                    ServiceRowsSelect::class,
                ],
                'Comments'  => [
                    Comment::class,
                ],
            ]),
        ];
    }

    public function save(Orders $orders)
    {
        $orders->fill($this->request->get('orders'))->save();
        Alert::info('Заявка сохранена');

        return redirect()->refresh();
    }

    public function remove(Orders $orders)
    {
        $orders->delete();
        Alert::info('Заявка удалена');

        return redirect()->route('dashboard.ordersystem.orders.list');
    }


    public function addService (Orders $orders, Request $request) {
        $service = $request->all();
        $orders->services()->attach($service['service_id']);
        Alert::info('Новая услуга добавлена');
        return redirect()->back();
    }

    public function fullPrice(Orders $orders, Request $request)
    {
        $sum = $orders->services()->sum('price');
        $orders->full_price = $sum;
        $orders->status_ended = 1;
        $orders->fill($this->request->get('orders'))->save();
        Alert::info('Счет выставлен. Заявка закрыта. Стоимость выполненых работ ' . $sum . ' р.');

        return redirect()->refresh();
    }

    public function addComments (Orders $orders, Request $request) {
        $comment = dtkComments::create([
          'user_id' => Auth::id(),
          'order_id'  => $orders->id,
          'text' => $request->text,
        ]);
        Alert::info('Новый коментарий добавлен');
        return redirect()->back();
    }
}
