<?php

namespace App\Http\Controllers\Screens\OrderSystem\Orders;

use App\Layouts\OrderRows;
use App\Layouts\OrderListLayout;
use App\Orders;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;
use Illuminate\Support\Facades\Auth;

class OrdersList extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Заявки';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Список полученных заявок от пользователей';

    

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'orders' => Orders::orderBy('id', 'Desc')->paginate(),
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Добавить заявку')->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            OrderListLayout::class,
            Layouts::modals([
                'create' => [
                    OrderRows::class,
                ],
            ]),
        ];
    }

    public function create(Request $request)
    {
        $currentUser = Auth::id();
        $orders = Orders::create(['user_id' => $currentUser]);

        return redirect()->route('dashboard.ordersystem.orders.edit',$orders->id);
    }
}
