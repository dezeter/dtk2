<?php

namespace App\Http\Controllers\Screens\OrderSystem\Orders;

use App\Orders;
use App\Services;
use App\Layouts\OrderRows;
use App\Layouts\ServiceListLayout;
use App\Layouts\ServiceRowsSelect;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Facades\Alert;


class OrderCreate extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'OrderCreate';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'OrderCreate';

    /**
     * Query data
     *
     * @return array
     */
    public function query($orders = null) : array
    {
        $orders = is_null($orders) ? new Orders() : $orders;

        return [
            'orders'     => $orders,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Сохранить')->method('save')
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            OrderRows::class,
        ];
    }

    public function save(Orders $orders)
    {
        $orders->fill($this->request->get('orders'))->save();
        Alert::info('Message');

        return redirect()->route('dashboard.ordersystem.orders.list');
    }
}
