<?php

namespace App\Http\Controllers\Screens\OrderSystem\Orders;

use App\Status;
use App\Orders;
use App\OrdersServices;
use App\Layouts\StatusRowsSelect;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Facades\Alert;

class OrdersListEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Изминение статуса заявки';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data
     *
     * @return array
     */
    public function query($ordersservices = null) : array
    {
        $ordersservices = is_null($ordersservices) ? new OrdersServices() : $ordersservices;
        
        return [
            'ordersservices' => $ordersservices,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Сохранить статус')->method('save'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            StatusRowsSelect::class,
        ];
    }

    public function save(OrdersServices $ordersservices) {

        $ordersservices->fill($this->request->get('ordersservices'))->save();
        Alert::info('Статус изменен!');

        return redirect()->route('dashboard.ordersystem.orders.edit', $ordersservices->orders_id );
    }
}
