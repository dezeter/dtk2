<?php

namespace App\Http\Controllers\Screens\OrderSystem\Orders;

use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;

class commentsList extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'commentsList';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'commentsList';

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [];
    }
}
