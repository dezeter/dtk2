<?php

namespace App\Http\Controllers\Screens\OrderSystem\Services;

use App\Services;
use App\Layouts\ServiceRows;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Facades\Alert;

class ServicesEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Услуги';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Создание/Редактирование услуг';

    /**
     * Query data
     *
     * @return array
     */
    public function query($services = null) : array
    {   
        return [
            'services' => is_null($services) ? new Services() : $services,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Сохранить')->method('save'),
            Link::name('Удалить')->method('remove'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            ServiceRows::class,
        ];
    }

    public function save(Services $services)
    {
        $services->fill($this->request->get('services'))->save();
        Alert::info('Услуга добавлена');

        return redirect()->route('dashboard.ordersystem.services.list');
    }

    public function remove(Services $services)
    {
        $services->delete();
        Alert::info('Услугша удалена');

        return redirect()->route('dashboard.ordersystem.services.list');
    }
}
