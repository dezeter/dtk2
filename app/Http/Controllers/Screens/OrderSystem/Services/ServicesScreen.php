<?php

namespace App\Http\Controllers\Screens\OrderSystem\Services;

use App\Services;
use App\Layouts\ServiceListLayout;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;

class ServicesScreen extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Услуги';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Список услуг';

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'services' => Services::orderBy('id', 'Desc')->paginate(),
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Создать')->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            ServiceListLayout::class,
        ];
    }

    public function create()
    {        
        return redirect()->route('dashboard.ordersystem.services.create');
    }
}
