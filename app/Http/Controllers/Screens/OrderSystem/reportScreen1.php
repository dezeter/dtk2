<?php

namespace App\Http\Controllers\Screens\OrderSystem;

use App\Orders;
use App\OrdersServices;
use App\dtkComments;
use App\User;
use App\Layouts\ReportOrders;
use App\Layouts\ReportOrdersActive;
use App\Layouts\ReportOrdersEnd;
use App\Layouts\ReportAll;
use App\Layouts\ReportEnded;
use App\Layouts\ReportComments;
use App\Layouts\ReportClients;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Layouts;

class reportScreen1 extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Отчет о заявках';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Вся информация о заявках';

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        $orders = Orders::all();
        $ended = OrdersServices::where('ended', 1)->count();
        $all = OrdersServices::all()->count();
        $allcomments = dtkComments::all()->count();
        $clients = User::all()->count()  - 1;

        return [
            'orders' => [
                'count' => $orders,
            ],
            'ended' => [
                'ended' => $ended,
            ],
            'all' => [
                'all'   => $all,
            ],
            'allomments' => [
                'allcomments'   => $allcomments,
            ],
            'clients' => [
                'clients'   => $clients,
            ],
            
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            Layouts::columns([
                '1' => [
                    ReportOrders::class,
                ],
                '2' => [
                    ReportOrdersActive::class,
                ],
                '3' => [
                    ReportOrdersEnd::class,
                ],
            ]),
            
        ];
    }
}
