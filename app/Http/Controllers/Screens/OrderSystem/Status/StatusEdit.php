<?php

namespace App\Http\Controllers\Screens\OrderSystem\Status;

use App\Status;
use App\Layouts\StatusRows;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Facades\Alert;

class StatusEdit extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Статус';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Создание/Редактирование статуса';

    /**
     * Query data
     *
     * @return array
     */
    public function query($status = null) : array
    {
        return [
             'status' => is_null($status) ? new Status() : $status,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Сохранить')->method('save'),
            Link::name('Удалить')->method('remove'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            StatusRows::class,
        ];
    }

    public function save(Status $status)
    {
        $status->fill($this->request->get('status'))->save();
        Alert::info('Статуст сохранен');

        return redirect()->route('dashboard.ordersystem.status.list');
    }

    public function remove(Status $status)
    {
        $status->delete();
        Alert::info('Статус успешно удалён');

        return redirect()->route('dashboard.ordersystem.status.list');
    }
}
