<?php

namespace App\Http\Controllers\Screens\OrderSystem\Status;

use App\Status;
use App\Layouts\StatusListLayout;
use Illuminate\Http\Request;
use Orchid\Platform\Screen\Screen;
use Orchid\Platform\Screen\Link;

class StatusScreen extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Список статусов';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'status' => Status::orderBy('id', 'Desc')->paginate(),
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar() : array
    {
        return [
            Link::name('Создать')->method('create'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout() : array
    {
        return [
            StatusListLayout::class,
        ];
    }

    public function create()
    {        
        return redirect()->route('dashboard.ordersystem.status.create');
    }
}
