<?php

namespace App\Http\Controllers;

use App\Status;
use App\Orders;
use App\OrdersServices;
use App\dtkComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function index() {
        if (Auth::check() == true) {
            $orders = Orders::where('user_id', Auth::id())->get();
            return view('main', [
                'orders' => $orders
            ]);
        } else {
            return redirect('login');
        }
            
    }

    public function show($id){
    	$status = Orders::with('order_services')->find($id);
        $dtkcomments = dtkComments::all()->where('order_id', $status->id);
        return view('orders.show', [
            'status' => $status,
            'comments' => $dtkcomments,
        ]);
    }

    public function store(Request $request) {
        $comment = dtkComments::create([
            'text'    => $request->text,
            'user_id' => $request->user()->getKey(),
            'order_id'=> $request->order_id,
        ]);
        return redirect(route('orders.order', $request->order_id));
    }

    
}
