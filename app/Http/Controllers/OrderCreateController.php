<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Services;
use App\OrdersServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OrderCreateController extends Controller
{
    public function index(Request $request) {
      $order = Orders::where('id', $request->route('id'))->first();
      return view('create', [
      	  'order' => $order,
          'services' => Services::all(),
      ]);
    }

    public function create(){
    	$currentUser = Auth::id();
      $order = Orders::create(['user_id' => $currentUser]);
      return redirect()->route('orders.create', $order->id);
    }

    public function addserv (Request $request) {
      $os = OrdersServices::create([
          'orders_id'  => $request->orders_id,
          'services_id' => $request->services_id,
      ]);
      return redirect(route('orders.create', $request->orders_id));
    }

    public function saveorder (Request $request) {
    	$order = Orders::find($request->orders_id);
    	$order->order_discription = $request->order_discription;
    	$order->save();
    	return redirect(route('orders.order', $request->orders_id));
    }

}
