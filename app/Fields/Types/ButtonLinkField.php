<?php

namespace App\Fields\Types;
use Orchid\Platform\Fields\Field;
class ButtonLinkField extends Field
{
    /**
     * @var string
     */
    public $view = 'fields.button-link';
    /**
     * Required Attributes.
     *
     * @var array
     */
    public $required = [
        'name', 'href'
    ];
    /**
     * Default attributes value.
     *
     * @var array
     */
    public $attributes = [
        'class' => 'btn',
        'style' => 'margin-bottom: 10px;',
    ];
    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    public $inlineAttributes = [
        'accept',
        'accesskey',
        'autocomplete',
        'autofocus',
        'checked',
        'disabled',
        'form',
        'formaction',
        'formenctype',
        'formmethod',
        'formnovalidate',
        'formtarget',
        'list',
        'max',
        'maxlength',
        'min',
        'multiple',
        'name',
        'pattern',
        'placeholder',
        'readonly',
        'required',
        'size',
        'src',
        'step',
        'tabindex',
        'href'
    ];
}