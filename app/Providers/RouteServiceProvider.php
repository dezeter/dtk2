<?php

namespace App\Providers;

use App\Orders;
use App\Status;
use App\Services;
use App\OrdersServices;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        Route::bind('services', function ($value) {
            return Services::firstOrNew(['id'=>$value]);
        });

        Route::bind('status', function ($value) {
            return Status::firstOrNew(['id'=>$value]);
        });

        Route::bind('orders', function ($value) {
            return Orders::firstOrNew(['id'=>$value]);
        });

        Route::bind('ordersservices', function ($value) {
            return OrdersServices::firstOrNew(['id'=>$value]);
        });
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));

        Route::middleware(config('platform.middleware.private'))
             ->namespace('App\http\Controllers\Screens')
             ->prefix('dashboard/screen')
             ->group(base_path('routes/screen.php'));
    }
}
