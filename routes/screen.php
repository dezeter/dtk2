<?php

$this->group(['namespace' => 'OrderSystem'], function($route)
{
	$route->screen('services/create', 'Services\ServicesEdit', 'dashboard.ordersystem.services.create');
	$route->screen('services/{services}/edit', 'Services\ServicesEdit', 'dashboard.ordersystem.services.edit');
	$route->screen('services', 'Services\ServicesScreen', 'dashboard.ordersystem.services.list');
	
	$route->screen('status/create', 'Status\StatusEdit', 'dashboard.ordersystem.status.create');
	$route->screen('status/{status}/edit', 'Status\StatusEdit', 'dashboard.ordersystem.status.edit');
	$route->screen('status', 'Status\StatusScreen', 'dashboard.ordersystem.status.list');

	$route->screen('orders/{orders}/edit', 'Orders\OrderEdit', 'dashboard.ordersystem.orders.edit');
	$route->screen('orders', 'Orders\OrdersList', 'dashboard.ordersystem.orders.list');

	$route->screen('ordersservices/{ordersservices}/edit', 'Orders\OrdersListEdit', 'dashboard.ordersystem.ordersservices.edit');

	$route->screen('ordersservices/reports1', 'reportScreen1', 'dashboard.ordersystem.reports1');
	$route->screen('ordersservices/reports2', 'reportScreen2', 'dashboard.ordersystem.reports2');
	$route->screen('ordersservices/reports3', 'reportScreen3', 'dashboard.ordersystem.reports3');

});
