<?php

use App\Orders;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::get('/', function () {
    return redirect('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/orders', 'OrdersController@index');

Route::get('/orders/{id}', 'OrdersController@show')->name('orders.order');
Route::post('/orders/{id}', 'OrdersController@store')->name('orders.store');
Route::get('/create', 'OrderCreateController@create')->name('orders.add');
Route::get('/create/{id}', 'OrderCreateController@index')->name('orders.create');
Route::post('/create/{id}', 'OrderCreateController@addserv')->name('orders.addserv');
Route::post('/create/{id}/save', 'OrderCreateController@saveorder')->name('orders.saveorder');

Route::get('/dashboard/reports', 'Reports@index')->name('layouts.reports');


