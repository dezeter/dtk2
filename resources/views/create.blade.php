@extends('layouts.app')

@section('content')
<div class="uk-container uk-margin">
<div id="app" class="uk-container uk-margin">

<div class="uk-card uk-card-default uk-card-body uk-width-1-1">
<h1 class="uk-heading-line uk-text-center"><span>Создание новой заявки</span></h1>
<div>
    <table class="uk-table uk-table-divider">
      <thead>
        <tr>
            <th>Название услуги</th>
            <th>Стоимость</th>
        </tr>
    </thead>
        @foreach($order->services as $service)
        <tr>
          <td>{{$service->service_name}}</td>
          <td>{{$service->price}}</td>
        </tr>
        @endforeach
    </table>
    <button class="uk-button uk-button-default uk-align-center" uk-toggle="target: #my-id" type="button">Добавить услугу</button>
    <h3 class="uk-text-center">Дополнительная информация</h3>
    <form action="{{route('orders.saveorder', $order->id)}}" method="post">
      {!! csrf_field() !!}
      <input type="hidden" name="orders_id" value="{{$order->id}}">
      <textarea  class="uk-textarea uk-align-center" name="order_discription" id="" cols="90" rows="5" value="order_discription" placeholder="Здесь вы можене внести дополнительную информацию, которую вы считает нужной."></textarea>
      <button class="uk-button uk-button-primary uk-align-center" type="submit">Сохранить заявку</button>
    </form>
</div>

<div id="my-id" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Выбор услуги</h2>
          <form action="{{route('orders.addserv', $order->id)}}" method="post">
            {!! csrf_field() !!}
            <input type="hidden" name="orders_id" value="{{$order->id}}">
            <select class="uk-select" name="services_id">
            @foreach($services as $service)
              <option value="{{$service->id}}">
                {{$service->service_name}}
              </option>
            @endforeach
            </select>
            <button type="submit" class="uk-button uk-button-default uk-align-center uk-button-secondary uk-width-1-2">Добавить</button>
          </form>
          <br>
        
    </div>
</div>

</div>
</div>
</div>
@endsection