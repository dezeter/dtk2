<div class="form-group{{ $errors->has($oldName) ? ' has-error' : '' }}">
    <a @include('dashboard::partials.fields.attributes', ['attributes' => $attributes])
           @if(isset($required) && $required) required @endif
    >{{ $title }}</a>
</div>
@include('dashboard::partials.fields.hr', ['show' => $hr ?? true])