@extends('layouts.app')

@section('content')
<div class="uk-container uk-margin">

<div class="uk-card uk-card-default uk-card-body uk-width-1-1">
	<div class="uk-card-header">
        <h3 class="uk-card-title">Заявка №{{$status->id}}</h3>
  </div>
   <div class="uk-card-body">
<table class="uk-table uk-table-divider">
	<thead>
        <tr>
            <th>Название услуги</th>
            <th>Стоимость</th>
            <th>Статус</th>
            <th>Время смены статуса</th>
        </tr>
    </thead>
	@foreach($status->order_services as $val)
		<tr>
			<td>{{$val->service->service_name}}</td>
			<td>{{$val->service->price}} р.</td>
			<td>{{$val->status->status_name}}</td>
			<td>{{$val->updated_at}}</td>
		</tr>
	@endforeach
</table>
</div>
<div class="uk-card-footer">
<div uk-alert>
    	<h4 class="uk-heading-line uk-text-center"><span>Дополнительная информация от клиента</span></h4>
    	<div>{{$status->order_discription}}</div>
</div>
@if ($status->status_ended == 1)
  <div>
        <div class="uk-card uk-card-primary uk-card-body">
            <h3 class="uk-card-title">Заявка закрыта.</h3>
            <p>Все работы выполнены в полном объеме.</p>
            <p>Сумма выполненных работ: <span class="uk-text-bold">{{$status->full_price}} BYN</span></p>
        </div>
    </div>
    

@endif 
</div>
</div>
<div class="uk-card uk-card-default uk-card-body uk-margin-top uk-width-1-1">
	<div class="uk-card-header">
        <h3 class="uk-card-title">Коментарии</h3>
  </div>
	<div class="uk-card-body">
		@foreach($comments as $comment)
		<article class="uk-comment">
    <header class="uk-comment-header">
        <h4 class="uk-comment-title">{{App\User::where('id', $comment->user_id)->first()->name}}</h4>
    </header>
    <div class="uk-comment-body">{{$comment->text}}</div>
	</article>
	<hr>
@endforeach
	</div>
  @if ($status->status_ended == 0)
  <div class="uk-card-footer">
  	<h2 class="uk-text-center"><span>Новый коментарий</span></h2>
  	<form action="{{route('orders.store', $status->id)}}" method="post">
  		{!! csrf_field() !!}
  		<input type="hidden" name="order_id" value="{{$status->id}}">
  		<textarea class="uk-textarea uk-align-center" name="text" id="" cols="90" rows="5" value="text"></textarea>
  		<div>
  		<button type="submit" class="uk-button uk-button-default uk-align-center uk-button-secondary uk-width-1-2">
  		  Оставть коментарий
			</button>
			</div>
  	</form>
  </div>
  @endif

</div>
</div>
@endsection