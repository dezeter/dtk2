@extends('layouts.app')

@section('content')
	<div  class="uk-container uk-margin">
		<div class="uk-card uk-card-default uk-card-body uk-width-1-1">
			<h1>Ваши заявки</h1>
			<div>
				<a href="{{route('orders.add')}}" class="uk-button uk-button-default uk-align-right">Добавить заявку</a>
				<table class="uk-table uk-table-divider">
					<thead>
        <tr>
            <th>Номер заявки</th>
            <th>Дополнительная информация</th>
            <th>Время создания</th>
        </tr>
    </thead>
			@foreach($orders as $order)
				<tr>
					<td><a class="uk-link-muted" href="{{route('orders.order', $order->id)}}">Заявка № {{$order->id}}</a></td>
					<td style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; max-width: 500px;">{{$order->order_discription}}</td>
					<td>{{$order->created_at}}</td>
				</tr>
		@endforeach
		</table>
		</div>
	</div>
@endsection